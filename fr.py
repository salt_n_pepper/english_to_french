
import sys
import numpy as np
import pickle
import tensorflow as tf

CODES = {'<PAD>': 0, '<EOS>': 1, '<UNK>': 2, '<GO>': 3 }

# Read files
with open('small_vocab_en', 'r', encoding='utf-8') as f:
    source_text =f.read()
with open('small_vocab_fr', 'r', encoding='utf-8') as f:
    target_text =f.read()

# Standardising
source_text = source_text.lower()
target_text = target_text.lower()

def create_lookup_tables(text):
    vocab = set(text.split())
    vocab_to_int = { v: v_i for v, v_i in CODES.items()}
    for v_i, v in enumerate(vocab, len(CODES)):
        vocab_to_int[v]=v_i
    int_to_vocab = { v_i: v for v, v_i in vocab_to_int.items()}
    return vocab_to_int, int_to_vocab

# Assigning ids to words in vocabulary
source_vocab_to_int, source_int_to_vocab = create_lookup_tables(source_text)
target_vocab_to_int, target_int_to_vocab = create_lookup_tables(target_text)

source_text = [[source_vocab_to_int[word] for word in line.split()] for line in source_text.split('\n')]

target_text = [[target_vocab_to_int[word] for word in line.split()] + [target_vocab_to_int['<EOS>']]for line in target_text.split('\n')]


# Save data
pickle.dump((
    (source_text, target_text),
    (source_vocab_to_int, target_vocab_to_int),
    (source_int_to_vocab, target_int_to_vocab)
), open('preprocessor.p', 'wb'))

source_maxn = max([len(s) for s in source_text])
target_maxn = max([len(s) for s in target_text])

from keras.preprocessing import sequence
from keras.models import Sequential
from keras.layers import Dense, Embedding, LSTM, RepeatVector, TimeDistributed, Activation

x_train = sequence.pad_sequences(np.array(source_text), maxlen=source_maxn, padding='post', value=0.)
y_train = sequence.pad_sequences(np.array(target_text), maxlen=target_maxn, padding='post', value=0.)


def process_data(abc):
    py=np.zeros((len(abc), target_maxn, len(target_vocab_to_int)))
    for i, sentence in enumerate(abc):
        for j, word in enumerate(sentence):
            py[i, j, word] = 1.
    return py


with tf.device('/gpu:0'):
    model = Sequential()
    model.add(Embedding(len(source_vocab_to_int),128,input_length=source_maxn,mask_zero=True))
    model.add(LSTM(128))  # encoder
    model.add(RepeatVector(target_maxn))  # Get the last output of the GRU and repeats it

    # Decoder
    
    model.add(LSTM(128, return_sequences=True))
    model.add(LSTM(128, return_sequences=True))
    model.add(LSTM(128, return_sequences=True))
    model.add(TimeDistributed(Dense(len(target_vocab_to_int))))
    model.add(Activation('softmax'))
    model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])

    for v in range(50):
        print(v) 
        indices = np.arange(len(y_train))
        np.random.shuffle(indices)
        x_t = x_train[indices[:3000]]
        y_t = y_train[indices[:3000]]

        labels=process_data(y_t)

        model.fit(x_t, labels, validation_split=0.1, batch_size=32, epochs=10)
    

    print("Input")
    f = open('test.txt', 'r')
    eng = f.read()
    f.close()
    eng = eng.lower()
    eng = [[source_vocab_to_int[word] for word in line.split()] for line in eng.split('\n')]

    print(eng)
    x_test = sequence.pad_sequences(np.array(eng), maxlen=source_maxn,padding='post')
    pred = model.predict_classes(x_test)
    
    for i in pred:
        fre = [target_int_to_vocab[word] for word in i]
        for y in fre:
            if y not in CODES:
                print(y,end=' ')
        print(' \n')